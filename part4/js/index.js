const NEWS_STORIES = [

    {
        "id": 1, 
        "title": "Amazon HQ Hunt", 
        "content": "Amazon is continuing its hunt for a new headquarters after maxing out Seattle.", 
        "image": "../images/amazon.png",
        "link": "https://amazon.com"
    },

    {
        "id": 2,
        "title": "Vancouver Banning Balloons?",
        "content": "The Vancouver Park Board is banning balloons in its parks. A protest is being held to contest the ban.",
        "image": "../images/balloon.jpg",
        "link": "http://vancouver.ca/your-government/vancouver-board-of-parks-and-recreation.aspx"
    }

]

$(document).ready(function() {
    
    /* Function that will be called when the document loads.
     * To dynamically insert into the DOM we need to do a few things:
     * 
     * 1. Create text. E.g if I wanted the text from PART 1 for the balloon
     * story I would do this:
     *
    var text =         
        '<img src="../images/balloon.jpg">
        <h4>Vancouver Banning Balloons?</h4>
        <p> The Vancouver Park Board is banning balloons in its parks. A protest
        is being held to contest the ban.
        </p>'
     * 
     * 2. Instead of hardcoding this text we want to generate this text for all
     * of our NEWS_STORIES and append the result of this all together.
     * Although string concatenation is not the best practice for this, we could
     * do the following to create the header:
     *
     * var text = '<h4>' + NEWS_STORIES[0].title + '</h4>' 
     *
     * 3. Append this to the correct location in the DOM.
     *
     * We can use jquery for this. Say we have two texts formatted as above, we
     * could append them serially one after another in the dom by:
     * $("#container").append(text, text2)
     *
     */

});



